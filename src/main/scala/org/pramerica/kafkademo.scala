package org.pramerica

import java.util
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer._
import java.util.Properties
import scala.collection.JavaConverters._

object kafkademo {

  def main(args: Array[String]): Unit = {

    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("auto.offset.reset", "latest")
    props.put("group.id", "consumer-group")

    consumeFromKafka("prdata", props)
  }

  def consumeFromKafka(topic: String, props: Properties) = {

    val consumer: KafkaConsumer[String, String] = new KafkaConsumer[String, String](props)
    consumer.subscribe(util.Arrays.asList(topic))
    while (true) {
      val record = consumer.poll(1000).asScala
      var reason = ""
      var quote = 500;
      for (data <- record.iterator)
        if ((data.age >= 18 && data.age <= 39) && (BMICal(data.ht, data.wt) <= 17.49 && BMICal(data.ht, data.wt) >= 38.5)) {
          quote = 750
          reason = "Age is between 18 to 39 and BMI is less than 17.49 or above 38.5"
        }
        else if ((data.age >= 40 && data.age <= 59) && (BMICal(data.ht, data.wt) <= 18.49 && BMICal(data.ht, data.wt) >= 38.5)) {
          quote = 1000
          reason = "Age is between 18 to 39 and BMI is less than 17.49 or above 38.5"
        }
        else if ((data.age >= 60) && (BMICal(data.ht, data.wt) <= 18.49 && BMICal(data.ht, data.wt) >= 38.5)) {
          quote = 2000
          reason = "Age is between 18 to 39 and BMI is less than 17.49 or above 38.5"
        }
        else {
          quote = 500
          reason = "BMI is in right range"
        }

      if (data.gender == "Female") {
        quote = .9 * quote;
      }

      var quateUSD = quote + " USD"
      var quoteDetails = new JSONObject(obj: Map[String, Any])
      quoteFull.appID = data.AppID
      quoteFull.rate = quote
      quoteFull.reason = reason

      writeToKafka("prresult", props, quoteDetails)
    }
  }

  def writeToKafka(topic: String, props: Properties, quoteDetails: Object): Unit = {

    val producer = new KafkaProducer[String, String](props)
    val record = new ProducerRecord[String, String](topic, quoteDetails.appID, quoteDetails)
    producer.send(record)
    producer.close()
  }

  def BMICal(ht: String, wt: String): Double = {
    var heightIn = ht.charAt(0).toInt * 12 + ht.charAt(1).toInt + ht.charAt(2).toInt
    var heightM = heightIn * 0.0254
    var BmiIndex = wt.toInt / (heightM * heightM)
    return BmiIndex;

  }
}
