package org.pramerica

import org.apache.spark.{SparkConf, SparkContext}

object sparkHello {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setMaster("local").
      setAppName("sparkHello")
    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val helloWorldString = "Hello World! spark"
    print(helloWorldString)

  }
}