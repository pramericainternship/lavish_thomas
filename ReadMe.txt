Camel Router with Scala DSL Project
===================================

To build this project use

    mvn install

To run this project

    spark-submit --class org.pramerica.sparkHello --master local[8] Intern.jar
    
For more help see the Apache Camel documentation

    http://camel.apache.org/

